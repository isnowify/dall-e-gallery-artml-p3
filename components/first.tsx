import { useState } from 'react';
import { CardContent, CardFooter, Card } from '@/components/ui/card';
import { Button } from '@/components/ui/button';
import Image from 'next/image';
// @ts-ignore
export default function First({ cards }) {
    const [currentCardIndex, setCurrentCardIndex] = useState(0);
    const [isFlipped, setIsFlipped] = useState(false);

    const handleFlip = () => {
        setIsFlipped(!isFlipped);
    };

    const handleNext = () => {
        if (currentCardIndex < cards.length - 1) {
            setCurrentCardIndex(currentCardIndex + 1);
            setIsFlipped(false);
        }
    };

    const handlePrevious = () => {
        if (currentCardIndex > 0) {
            setCurrentCardIndex(currentCardIndex - 1);
            setIsFlipped(false);
        }
    };

    const { frontImage, backImage, frontText, backText , storyUrl} = cards[currentCardIndex];

    // @ts-ignore
    return (
        <div className="w-full max-w-2xl">
            <div className="p-4">
                <Card>
                    <CardContent className={`flex flex-col items-center justify-center p-8 ${isFlipped ? 'hidden' : ''}`} style={{ minHeight: '400px' }}> {/* Increased padding and minimum height */}
                        <Image
                            alt="Front Cover"
                            className="rounded-lg" // Applied object-cover style
                            width={400}
                            height={400}
                            src={frontImage}
                        />
                        <p className="text-md text-left leading-tight mt-4">{frontText}</p> {/* Text will now be to the right of the image */}
                    </CardContent>
                    <CardContent className={`flex flex-col items-center justify-center p-8 ${isFlipped ? '' : 'hidden'}`} style={{ minHeight: '400px' }}> {/* Increased padding and minimum height */}
                        <Image
                            alt="Back Cover"
                            className="rounded-lg" // Applied object-cover style
                            width={400}
                            height={400}
                            src={backImage}
                        />
                        <p className="text-md text-left leading-tight mt-4">{backText}</p> {/* Text will now be to the right of the image */}
                    </CardContent>
                    <CardFooter className="flex align-middle justify-center p-4">
                        <Button className="w-12 h-12 mr-2" size="icon" variant="ghost" onClick={handlePrevious}>
                            <ChevronLeftIcon className="w-8 h-8" />
                        </Button>
                        <Button className="w-14 h-14" variant="ghost" onClick={handleFlip}>
                            <FlipVerticalIcon className="w-8 h-8" />
                        </Button>
                        <Button className="w-10 h-10" size="icon" variant="ghost" onClick={() => {
                            // @ts-ignore
                            return window.open(storyUrl, '_blank');
                        }}>
                            <UrlIcon className="w-8 h-8" />
                        </Button>
                        <Button className="w-12 h-12 ml-2" size="icon" variant="ghost" onClick={handleNext}>
                            <ChevronRightIcon className="w-8 h-8" />
                        </Button>
                    </CardFooter>
                </Card>
            </div>
        </div>
    );
}




// @ts-ignore
function ChevronLeftIcon(props) {
    return (
        <svg
            {...props}
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="m15 18-6-6 6-6" />
        </svg>
    )
}

// @ts-ignore
function ChevronRightIcon(props) {
    return (
        <svg
            {...props}
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="m9 18 6-6-6-6" />
        </svg>
    )
}

// @ts-ignore

function FlipVerticalIcon(props) {
    return (
        <svg
            {...props}
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="M21 8V5a2 2 0 0 0-2-2H5a2 2 0 0 0-2 2v3" />
            <path d="M21 16v3a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-3" />
            <path d="M4 12H2" />
            <path d="M10 12H8" />
            <path d="M16 12h-2" />
            <path d="M22 12h-2" />
        </svg>
    )
}

// @ts-ignore
function UrlIcon(props) {
    return (
        <svg
            {...props}
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="M10 13a5 5 0 0 0 7.54.54l3-3a5 5 0 0 0-7.07-7.07l-1.72 1.71"/>
            <path d="M14 11a5 5 0 0 0-7.54-.54l-3 3a5 5 0 0 0 7.07 7.07l1.71-1.71"/>
        </svg>
    )
}
