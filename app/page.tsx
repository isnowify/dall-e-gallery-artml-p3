'use client'
import Link from "next/link"
import Image from "next/image";
import First from "@/components/first";

export default function HomePage() {
  const cardData = [
    {
      frontImage: "/image8.png",
      backImage: "/image2.png",
      storyUrl: "https://www.understood.org/en/articles/teen-with-adhd-builds-a-fidget-empire",
      frontText: "Many individuals with ADHD face daily challenges that are often misunderstood. Allan Maman's experience highlights this: despite his intelligence, he struggles with focus amidst distractions like noisy environments. His messy room and difficulty with routine tasks reflect ADHD's complexity, not laziness. These misunderstandings can lead to criticism and isolation for people like Allan.",
      backText: "Allan Maman transformed his ADHD challenges into entrepreneurial success by creating Fidget360, driven by his ADHD-induced energy and focus. His innovative designs, including the unique \"Batman\" spinner, differentiated his products in the competitive market. Starting in a high school lab and expanding to a Brooklyn factory, Allan's venture showcases how ADHD traits like creativity and hyperfocus can become strengths, leading to over $350,000 in profits and international recognition. ",
    },
    {
      frontImage: "/image4.png",
      backImage: "/image3.png",
      storyUrl: "https://www.chop.edu/stories/anxiety-and-adhd-molly-s-story",
      frontText: "Before receiving necessary support, Molly struggled with confusion, anxiety, and isolation, worsened by the COVID-19 pandemic. Facing undiagnosed ADHD and dyslexia, school became overwhelmingly difficult, with each task feeling insurmountable. The transition to remote learning removed essential structure, exacerbating her lack of focus and increasing anxiety, particularly regarding her peers' health. This time was characterized by intense anxiety, academic pressure, and deep loneliness from feeling misunderstood.",
      backText: "Diagnosed and treated for ADHD at the Children's Hospital of Philadelphia, Molly benefited from a tailored plan including therapy and text-to-speech software. This significantly improved her academic performance and social interactions. Her newfound confidence was evident in swimming successes. Molly's story underscores the impact of targeted support for children with ADHD and anxiety.",
    },
    {
      frontImage: "/image11.png",
      backImage: "/image5.png",
      storyUrl: "https://www.understood.org/en/articles/astronaut-scott-kelly-opens-up-about-his-attention-issues",
      frontText: "Scott Kelly, a record-holding astronaut, faced significant challenges in his early life, including a troubled childhood and academic struggles. However, reading Tom Wolfe's \"The Right Stuff\" inspired him to pursue a career in aviation and space exploration. Despite attention issues similar to ADHD, Kelly found that these challenges helped him prioritize and focus on critical tasks during his space missions, turning a potential weakness into a strength.",
      backText: "Scott Kelly, a former astronaut, overcame childhood attention issues to become a space traveler. Inspired by the book \"The Right Stuff,\" he joined the Navy ROTC, earned an engineering degree, and became a Navy pilot. Kelly completed four space missions, including a year-long stay on the ISS, and spent a total of 382 days in space. He is now retired from NASA and working on a memoir, \"Endurance: My Year in Space and Our Journey to Mars,\" which will also be adapted into a movie by Sony Pictures.",
    },
    {
      frontImage: "/image6.png",
      backImage: "/image7.png",
      storyUrl: "https://www.additudemag.com/you-throw-girl-adhd-role-model-wins-gold-in-rio/",
      frontText: "Michelle Carter struggled with ADHD and dyslexia from a young age, which affected her academic performance. Despite her high IQ, she had difficulty focusing and keeping pace with her peers. Carter briefly tried medication for ADHD, but it led to an excessive focus on mundane tasks, so she decided to stop taking it.",
      backText: "Michelle Carter, an Olympic gold medalist in shot put, broke the American record at the Rio Olympics, becoming the first American woman to win the event. She was a standout athlete from a young age, receiving a scholarship to the University of Texas. Coached by her father, Michael Carter, they became the first father-daughter team to medal in the Olympics in the same sport.",
    },
    {
      frontImage: "/image10.png",
      backImage: "/image12.png",
      storyUrl: "https://www.understood.org/en/articles/celebrity-spotlight-why-william-says-adhd-fuels-his-creativity/",
      frontText: "Will.i.am grew up in a tough neighborhood near Los Angeles with ADHD. Often described as \"hyper,\" his mother worked hard to steer him away from gangs and toward artistic pursuits. Music became his tool to manage ADHD, helping him focus and control his thoughts. With his mother's support, he channeled his creativity into forming the successful hip-hop group, the Black Eyed Peas.",
      backText: "Will.i.am, of the Black Eyed Peas, credits his ADHD for his creativity and success in music and other fields. As an inventor, tech entrepreneur, and philanthropist, he founded the i.am.angel Foundation to support underprivileged youth in STEAM education. His journey illustrates how challenges like ADHD can be turned into opportunities for growth and success.",
    },
  ];

  return (
      <div className="flex flex-col h-screen">
        <header className="border-b p-4 dark:border-gray-800">
          <div className="container mx-auto">
            <nav className="flex items-center space-x-4">
              <Link className="flex items-center space-x-2 text-lg font-semibold" href="#">
                Artful Empowerment: ADHD Insights
              </Link>
            </nav>
          </div>
        </header>
        <main className="flex justify-center items-center h-full"> {/* Changed h-screen to h-full and removed p-4 if there was any*/}
          <First cards={cardData}/>
        </main>
      </div>
  );

}